
# https://www.eea.europa.eu/data-and-maps/data/natura-11/natura-2000-tabular-data-12-tables/natura-2000-access-database/at_download/file
import pandas as pd
import pandas_access as mdb
import sqlalchemy
import yaml

eu_dir='./eu'

#with open("natura2000.yaml", "r") as yamlfile:
#    data = yaml.load(yamlfile, Loader=yaml.FullLoader)
with open("params.yaml", "r") as params:
    data = yaml.safe_load(params)

#print(data['databases'])

engine_uri = 'postgresql://n2k:changeMe@localhost:45452/n2k'
engine = sqlalchemy.create_engine(engine_uri)


df = pd.read_excel(eu_dir + '/DINPI/2020_egyebfontosfajokkal_DINPI.xlsx', sheet_name='SDF_élőhelyek') #, index_col=0
df.columns=["HABITAT_ID","SITE_CODE","Site name","HABITAT_CODE","HABITAT_NP","HABITAT_COVER_HA","HABITAT_CAVES","HABITAT_DATA_QUALITY","HABITAT_REPRESENTATIVITY","HABITAT_RELATIVE_SURFACE","HABITAT_CONSERVATION","HABITAT_GLOBAL","Módosítandó adat típusa","Régi adat","Módosított adat","Adatforrás","Módszer","Megjegyzés"]

schema_name = 'DINPI'

if not engine.dialect.has_schema(engine, schema_name):
  engine.execute(sqlalchemy.schema.CreateSchema(schema_name))

df.to_sql('teszt', engine, schema=schema_name, if_exists='replace')

exit()

for database in data['databases']:
  print(database)
#  if data['databases'][database]['import']:
  if database['import']:
#    print(data['databases'][database])
#    print(data['databases'][database]['tables'])
#    for table in data['databases'][database]['tables']:
#      print(table)

#    TABLES = data['databases'][database]['tables']

    schema_name = database['name']
    mdb_file=eu_dir + "/" + database['name'] + ".mdb"

#from sqlalchemy import create_engine

# for create database or schema
# https://stackoverflow.com/questions/50927740/sqlalchemy-create-schema-if-not-exists
#from sqlalchemy_utils.functions import database_exists, create_database

#    for tbl in mdb.list_tables(mdb_file):
#      print(tbl)

#TABLES=[ "BIOREGION", "DESIGNATIONSTATUS", "DIRECTIVESPECIES", "HABITATCLASS",
#         "HABITATS", "IMPACT", "MANAGEMENT", "METADATA", "NATURA2000SITES", "OTHERSPECIES",
#         "SPECIES" ]

#   TABLES=[ "BIOREGION", "NATURA2000SITES" ]


    if not engine.dialect.has_schema(engine, schema_name):
      engine.execute(sqlalchemy.schema.CreateSchema(schema_name))

    for tbl in database['tables']:
      print(tbl);
      df=pd.DataFrame()
      df=mdb.read_table(mdb_file, tbl['name'], converters_from_schema=False)

      if 'filter' in tbl:
        print(tbl['filter'])
        #df_in = df[df[tbl['filter']['column']].str.startswith(tbl['filter']['operand'])]
#        df_in = df.query('SITECODE < "HZ" and SITECODE > "HU"')
        df_in = df.query(tbl['filter']['query'])
      elif database['filter']:
        if database['filter']['column'] in df.columns:
          print(database['filter'])
          df_in = df.query(database['filter']['query'])
        else:
          df_in = df
      else:
        df_in = df
  #print(df.head())


      del df
      del df_in

