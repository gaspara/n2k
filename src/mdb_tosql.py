
# https://www.eea.europa.eu/data-and-maps/data/natura-11/natura-2000-tabular-data-12-tables/natura-2000-access-database/at_download/file
import pandas as pd
import pandas_access as mdb
import sqlalchemy
import yaml
import argparse


parser = argparse.ArgumentParser(description='Load Access database to Natura 2000 Postgresql database')
parser.add_argument('db', metavar='db', help='an integer for the accumulator')
parser.add_argument('--dir', metavar='dir', nargs=1 ,help='an integer for the accumulator')

args = parser.parse_args()
print(args.db)
print(args.dir)

space =' '

#with open("natura2000.yaml", "r") as yamlfile:
#    data = yaml.load(yamlfile, Loader=yaml.FullLoader)
with open("params.yaml", "r") as params:
    data = yaml.safe_load(params)

db=data[args.db]
if 'directory' in db:
  db_dir=db['directory']
else:
  db_dir='./'

#print(eu)
#for database in db['databases']:
#  print(database['name'])
#  if ('import' not in database and ( 'import' in db and db['import'])) or ('import' in database and database['import']):
#    print('import')
  

engine_uri = 'postgresql://n2k:changeMe@localhost:45452/n2k'
engine = sqlalchemy.create_engine(engine_uri)

for database in db['databases']:
  print(database)
  if ('import' not in database and ( 'import' in db and db['import'])) or ('import' in database and database['import']):

    schema_name = database['name']

    fn = database['name']
    if 'filename' in database:
      fn = database['filename']
    mdb_file=db_dir + "/" + fn + ".mdb"

    if not engine.dialect.has_schema(engine, schema_name):
      engine.execute(sqlalchemy.schema.CreateSchema(schema_name))
 
    tables = []
    if 'tables' in database:
      tables = database['tables']
    elif 'tables' in db:
      tables = db['tables']
    else:
      for table in mdb.list_tables(mdb_file):
        tables.append({'name': table})

    for tbl in tables:
      print( space + tbl['name']);
      df=pd.DataFrame()
      df=mdb.read_table(mdb_file, tbl['name'], converters_from_schema=False)
      df.columns = map(str.lower, df.columns)

      cond = ''
      if 'filter' in tbl:
        cond = tbl['filter']['query']
      elif 'filter' in database:
        if database['filter']['column'] in df.columns:
          cond = database['filter']['query']
      elif 'filter' in db:
        if db['filter']['column'] in df.columns:
          cond = db['filter']['query']
      if cond:
        print(cond)
        df_in = df.query(cond)
      else:
        df_in = df
      print( space + tbl['name'].lower())
      df_in.to_sql(tbl['name'].lower(), engine, schema=schema_name, if_exists='replace')
      del df
      del df_in
      print('')

