
{% macro eu_year_species_stat(year, country_code, season='end') %}

with species as (

  select
    {{ year }} year,
    sitecode,
    speciesname,
    spgroup,
    sensitive,
    population_type,
    lowerbound,
    upperbound,
    counting_unit::text as counting_unit,
    abundance_category::text,
    dataquality,
    population,
    conservation,
    isolation,
    global
  from {{ source(season + year, 'species')}} where country_code like '{{country_code}}'

)

select * from species


{% endmacro %}
