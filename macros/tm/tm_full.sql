
{% macro tm_full(year, population='%', season='end') %}

with species as (

  SELECT
   *
  FROM
    {{ ref(season + year + '_species') }}
  WHERE population like '{{population}}'

), matrix AS (

 select
   sitecode,
   {{ dbt_utils.pivot(
        'speciesname',
        dbt_utils.get_column_values(ref('species'), 'speciesname','speciesname'),
        true
   ) }}
 from
   species
 group by 1
--        agg='string_agg',
--       then_value='population' 

)

select * from matrix


{% endmacro %}