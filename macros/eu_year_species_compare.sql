{% macro eu_year_species_compare(yearA, yearB, country_code, seasonA='end', seasonB='end') %}

with compare as (

  {{ audit_helper.compare_relations(
      a_relation=ref(seasonA + yearA + '_species'),
      b_relation=ref(seasonB + yearB + '_species'),
      primary_key="md5"
    )
  }}

)

select * from compare

{% endmacro %}
