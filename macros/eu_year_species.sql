
{% macro eu_year_species(year, country_code, season='end') %}

with species as (

  select
    {{ dbt_utils.generate_surrogate_key(['sitecode','speciescode', 'population_type']) }},
    sitecode,
    speciesname,
    spgroup,
    sensitive,
    population_type,
    lowerbound,
    upperbound,
    counting_unit::text as counting_unit,
    abundance_category::text,
    dataquality,
    population,
    conservation,
    isolation,
    global
  from {{ source(season + year, 'species')}} where country_code like '{{country_code}}'

)

select * from species


{% endmacro %}
