# Official Natura2000 Database

For python environment management use conda. For initialize conda download installer:
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

Then run installer:
bash Miniconda3-latest-Linux-x86_64.sh

For more information see:
https://docs.conda.io/en/latest/miniconda.html#linux-installers

In first time run these commands to get database up and running:

```
cat ./profiles.yml >> ~/.dbt/profiles.yml
docker-compose up -d
dbt deps
dbt seed
dbt compile
dbt run
```

Without settings profiles
```
docker-compose up -d
dbt deps    --profile-dir ./
dbt seed    --profile-dir ./
dbt compile --profile-dir ./
dbt run     --profile-dir ./
```

Than open http://localhost:45453 in browser (look up login credentials in profiles.yml)

EU Natura 2000 public database download:
https://www.eea.europa.eu/data-and-maps/data/natura-14

