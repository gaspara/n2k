
with species as (

  select
    {{ dbt_utils.generate_surrogate_key(['sitecode','speciescode', 'population_type']) }},
    sitecode,
    speciesname,
    spgroup,
    sensitive,
    population_type,
    lowerbound,
    upperbound,
    counting_unit,
    abundance_category,
    dataquality,
    population,
    conservation,
    isolation,
    global
  from {{ source('end2017', 'species')}} where country_code like 'HU'

)

select * from species
