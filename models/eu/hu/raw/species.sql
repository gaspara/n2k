
with s2020 as (

  select '2020' as year, * from end2020_species

), s2019 as (

  select '2019' as year, * from end2019_species

), s2018 as (

  select '2018' as year, * from end2018_species

), s2017 as (

  select '2017' as year, * from end2017_species

), s2016 as (

  select '2016' as year, * from end2016_species

), s2015 as (

  select '2015' as year, * from end2015_species

), s2014 as (

  select '2014' as year, * from end2014_species

), s2013 as (

  select '2013' as year, * from end2013_species

), s2012 as (

  select '2012' as year, * from end2012_species

), s2021 as (

  select '2021' as year, * from {{ source('end2021', 'species') }}

), ossz as (

  select * from s2020
  union
  select * from s2019
  union
  select * from s2018
  union
  select * from s2017
  union
  select * from s2016
  union
  select * from s2015
  union
  select * from s2014

)


select * from ossz
