
with compare as (

  {{ audit_helper.compare_relations(
      a_relation=ref('end2015_species'),
      b_relation=ref('end2014_species'),
      primary_key="md5"
    )
  }}

)

select * from compare
