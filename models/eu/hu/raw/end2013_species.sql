
with species as (

  select
    {{ dbt_utils.generate_surrogate_key(['sitecode','speciescode', 'population_type']) }},
    sitecode,
    speciesname,
    spgroup,
    sensitive,
    population_type,
    lowerbound,
    upperbound,
    counting_unit::text as counting_unit,
    abundance_category::text,
    dataquality,
    population,
    conservation,
    isolation,
    global
  from {{ source('end2013', 'species')}} where country_code like 'HU'

)

select * from species
