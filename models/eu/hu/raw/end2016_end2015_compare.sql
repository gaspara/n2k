
with compare as (

  {{ audit_helper.compare_relations(
      a_relation=ref('end2016_species'),
      b_relation=ref('end2015_species'),
      primary_key="md5"
    )
  }}

)

select * from compare
