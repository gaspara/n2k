<?php
require_once('plugins/login-servers.php');

/** Set supported servers
* @param array array($description => array("server" => , "driver" => "server|pgsql|sqlite|..."))
* @param array array($domain) or array($domain => $description) or array($category => array())
* @param string
*/
return new AdminerLoginServers([
"N2K" => array( "server" =>"database", "driver"=> "pgsql" )
]);
